package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Lista;
import estructuras.Nodo;
import estructuras.TablaHash;
import estructuras.TablaHash.Tip;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";
	
	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";

	
	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	private GrafoNoDirigido<String,String> grafo;
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String

	
	
	
	
	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		grafo = new GrafoNoDirigido<String,String> ();
		//TODO inicialice el grafo como un GrafoNoDirigido
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		Arco<String, String>[] datos1 = grafo.darArcosDestino(identificador);
		Arco<String, String>[] datos2=grafo.darArcosOrigen(identificador);
		String estaciones= "";
		for(int i =0;i<datos1.length;i++)
		{
			estaciones+=datos1[i].darInformacion()+",";
		}
		for(int i =0;i<datos2.length;i++)
		{
			if(!estaciones.contains(datos2[i].darInformacion()))estaciones+=datos2[i].darInformacion()+",";
		}
		
		return estaciones.split(",");
		
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		Arco<String, String>[] arcos =grafo.darArcos();
		double resp =arcos[0].darCosto();
		for(int i=1;i<arcos.length;i++)
		{
			if(arcos[i].darCosto()<resp)
			{
				resp=arcos[i].darCosto();
			}
		}
		
		return resp;
	}
	
	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()
	{
		Arco<String, String>[] arcos =grafo.darArcos();
		double resp =arcos[0].darCosto();
		for(int i=1;i<arcos.length;i++)
		{
			if(arcos[i].darCosto()>resp)
			{
				resp=arcos[i].darCosto();
			}
		}
		
		return resp;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		BufferedReader br = new  BufferedReader(new FileReader(RUTA_PARADEROS));
		String actual = br.readLine();
		int tamanio= Integer.parseInt(actual);
		actual=br.readLine();
		while(actual!=null)
		{
			String [] datos = actual.split(";");
			double latitud=Double.parseDouble(datos[1]);
			double longitud=Double.parseDouble(datos[2]);
			Estacion<String> nueva = new Estacion<String>(datos[0], latitud, longitud);
			grafo.agregarNodo(nueva);
			actual = br.readLine();
		}
		
		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");
		
		br= new BufferedReader(new FileReader(RUTA_RUTAS));
		actual = br.readLine();
		int tamanioArco= Integer.parseInt(actual);
		actual  = br.readLine();
		while(actual!= null)
		{
			if(actual.equals("--------------------------------------"))
			{
				actual = br.readLine();
				String nombre= actual;
				actual=br.readLine();
				int numeroEst = Integer.parseInt(actual);
				actual = br.readLine();
				for(int i =0; i< numeroEst;i++)
				{
					String[]  dato =  actual.split(" ");
					String inicio =  dato[0];
					actual = br.readLine();
					if(actual==null)break;
					if(actual.equals("--------------------------------------")) break;
					dato =  actual.split(" ");
					double valor = Double.parseDouble(dato [1]);
					grafo.agregarArco(inicio,dato[0], valor,nombre);
				}

			}

		}
		
		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		return (Estacion<String>) grafo.buscarNodo(identificador);
	}

}

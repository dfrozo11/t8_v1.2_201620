package estructuras;

import java.util.ArrayList;


public class TablaHash<K ,V>
{
	public enum Tip 
	{
		Linear, 
		Chaining
	}

	private int M; 

	private int N;

	private K[] llaves;

	private V[] valores;

	public Tip tipo;

	private SecuentialSearchST<K, V>[] st;

	private int colisiones; 

	@SuppressWarnings("unchecked")


	public TablaHash(int Capacidad, Tip t)
	{

		tipo = t;
		this.M=Capacidad;
		if(Tip.Linear.equals(t))
		{
			llaves = (K[]) new Object[M];
			valores= (V[]) new Object[M];
		}
		else
		{
			st= (SecuentialSearchST<K, V>[]) new SecuentialSearchST[M];
			for(int i=0; i<M;i++)
			{
				st[i]= new SecuentialSearchST();
			}
		}

	}

	public void put(K llave, V valor){

		if(tipo == Tip.Linear)
		{
			if(N>=M/2) {resize(2*M);}
			int  i; 
			for (i = hash(llave); llaves[i]!= null; i=(i+1)%M)
			{
				if(llaves[i].equals(llave)){valores[i]=valor;return;}

			}

			llaves[i]=llave;
			valores[i]= valor;
			N++;

		}
		else
		{
			if(N>=M/2) resize(2*M);
			colisiones+=st[hash(llave)].put(llave, valor);
		}


	}

	public V get(K llave)
	{

		if(tipo == Tip.Linear)
		{
			for(int i = hash(llave); llaves[i]!=null; i =(i+1)%M)
			{
				if (llaves[i].equals(llave))
				{
					return valores[i];
				}
			}
			return null;
		}
		else
		{
			return (V) st[hash(llave)].getValor(llave);
		}
	}

	public void delete(K llave)
	{
		if(!contains(llave))return;
		if(tipo == Tip.Linear)
		{
			int i;
			for( i = hash(llave); llaves[i]!=null; i =(i+1)%M)
			{
				if (llaves[i].equals(llave))
				{
					llaves[i]= null;
					valores[i]=null;
				}
			}
			i = (i+1)%M;
			while(llaves[i]!=null)
			{
				K llaveARehacer = llaves[i];
				V valorARehacer= valores[i];
				llaves[i]= null;
				valores[i]=null;
				N--;
				put(llaveARehacer, valorARehacer);
				i = (i+1)%M;
			}
			N--;
			if(N>0 && N==M/8) resize(M/2);
		}
		else
		{
			if(st[hash(llave)].delete(llave)!=null)
			{
				N--;
				if(N>0 && N==M/8) resize(M/2);
			}
		}
	}

	//Hash
	private int hash(K llave)
	{
		int val =Math.abs((llave.hashCode()))%M;
		if(val<0)val*=-1;
		return val;
	}

	private void resize(int cap)
	{
		if(Tip.Linear.equals(tipo))
		{
			TablaHash <K, V> t;
			t = new TablaHash<K,V>(cap,tipo);
			for (int i = 0; i <M ; i++)
			{
				if (llaves[i]!= null)
				{	
					t.put(llaves [i], valores[i]);
				}

			}
			llaves = t.llaves;
			valores = t.valores;
			M = t.M;

		}
		else
		{
			TablaHash<K, V> t;
			t= new TablaHash<K,V>(cap,tipo);
			for(int i =0; i<M;i++)
			{
				ArrayList x =(ArrayList) st[i].keys();

				for(int j = 0; j < x.size() ; j++)
				{
					t.put((K) x.get(j),st[i].getValor((K) x.get(j)) );


				}
			}
			st=t.st;
			M=t.M;
		}
	}

	public boolean contains(K llave)
	{
		for(int i = hash(llave); llaves[i]!=null; i =(i+1)%M)
		{
			if (llaves[i].equals(llave))
			{
				return true;
			}
		}

		return false;

	}

	public int size()
	{
		return N;

	}

	public int darColisiones()
	{
		return colisiones;
	}

	public int darN() 
	{
		// TODO Auto-generated method stub
		return N;
	}

	public Lista<K> llaves()
	{
		Lista<K> keys = new Lista<K>();

		for(K llav : llaves )
		{
			if (llav!=null)keys.add(llav);
		}
		return keys;
	}
}

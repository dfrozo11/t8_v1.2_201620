package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import estructuras.TablaHash.Tip;
import modelos.Estacion;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	/**
	 * Nodos del grafo
	 */
	private TablaHash<K,Nodo<K>> nodos;
	//TODO Declare la estructura que va a contener los nodos

	/**
	 * Lista de adyacencia 
	 */
	private TablaHash<K, Lista<Arco>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
	// Es libre de implementarlo con la representacion de su agrado. 

	private ArrayList arcos;
	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() 
	{
		adj= new TablaHash<K, Lista<Arco>>(10, Tip.Linear);
		nodos =  new TablaHash<K,Nodo<K>> (1000, Tip.Linear);
		arcos = new ArrayList<>();
		//TODO implementar

	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo) {
		nodos.put(nodo.darId(),nodo);	
		adj.put( nodo.darId(), new Lista<Arco>());
		return true;
	}

	@Override
	public boolean eliminarNodo(K id) 
	{
		nodos.delete(id);
		return false;
	}

	@Override
	public Arco<K,E>[] darArcos() 
	{
		Arco[] resp = new Arco[arcos.size()];

		for( int i =0; i < resp.length;i++)
		{
			resp[i]= (Arco)arcos.get(i);
		}

		return resp;
		//TODO implementar
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos() {
		
		Lista<K> llaves=  (Lista<K>) nodos.llaves();
		Nodo<K>[] resp = (Nodo<K>[])new Nodo[llaves.size()];
		for(int i=0; i< llaves.size();i++)
		{
			resp[i]=nodos.get(llaves.get(i));
		}
		 return resp;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) {

		try{

			Arco nuevo =  crearArco(inicio, fin, costo, obj);
			adj.get(inicio).add(nuevo);
			adj.get(fin).add(nuevo);
			arcos.add(nuevo);

			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) {
			
		Lista actual =  adj.get(inicio);
		for( int i=0; i<actual.size();i++)
		{
			Arco  buscado =  (Arco) actual.get(i);
			if(buscado.darNodoInicio().darId().equals(inicio)&&buscado.darNodoFin().darId().equals(fin))
			{
				actual.remove(i);
				break;
			}
		}
		actual=adj.get(fin);
		for( int i=0; i<actual.size();i++)
		{
			Arco<K, E>  buscado =  (Arco<K, E>) actual.get(i);
			if(buscado.darNodoInicio().darId().equals(inicio)&&buscado.darNodoFin().darId().equals(fin))
			{
				actual.remove(i);
				return buscado;
			}
		}
		
		return null;
	}

	@Override
	public Nodo<K> buscarNodo(K id) {
		
		return nodos.get(id);
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		Lista<Arco> arcos=  new Lista<Arco>();
		Lista<Arco> aRecorrer=adj.get(id);
		for( int i=0;i<aRecorrer.size();i++)
		{
			Arco actual= aRecorrer.get(i);
			if(actual.darNodoInicio().darId().equals(id))
			{
				arcos.add(actual);
			}
		}

		Arco[] resp = new Arco[arcos.size()];

		for(int i =0; i< arcos.size();i++)
		{
			resp[i]=arcos.get(i);
		}
		return resp; 
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		//TODO implementar

		Lista<Arco> arcos=  new Lista<Arco>();
		Lista<Arco> aRecorrer=adj.get(id);
		for( int i=0;i<aRecorrer.size();i++)
		{
			Arco actual= aRecorrer.get(i);
			if(actual.darNodoFin().darId().equals(id))
			{
				arcos.add(actual);
			}
		}

		Arco[] resp = new Arco[arcos.size()];

		for(int i =0; i< arcos.size();i++)
		{
			resp[i]=arcos.get(i);
		}
		return resp; 
	}



	public boolean contieneArco(Arco aBuscar)
	{

		for( int i=0; i< arcos.size();i++)
		{
			Arco actual =(Arco)arcos.get(i);
			if(actual.darNodoFin().equals(aBuscar.darNodoFin())&&actual.darNodoInicio().equals(aBuscar.darNodoInicio())&&actual.darCosto()==aBuscar.darCosto())
			{
				return true;
			}
		}
		return false;


	}
}


